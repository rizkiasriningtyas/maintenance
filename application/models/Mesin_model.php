<?php
class Mesin_model extends CI_Model
{
 function fetch_all()
 {
  $this->db->order_by('id_mesin', 'DESC');
  return $this->db->get('mesin');
 }

 function insert_api($kode_mesin,$data)
 {
  // $this->db->insert('mesin', $data);
  // if($this->db->affected_rows() > 0)
  // {
  //  return true;
  // }
  // else
  // {
  //  return false;
  // }

  $this->db->where('kode_mesin', $kode_mesin);
  $q = $this->db->get('mesin');

  if($q->num_rows() < 1 ){
    $this->db->insert('mesin', $data);
    if($this->db->affected_rows() > 0)
    {
     return true;
   }else{
     return false;
   }
  }
  else{
     return false;
  }
 }

 function fetch_single_user($id_mesin)
 {
  $this->db->where("id_mesin", $id_mesin);
  $query = $this->db->get('mesin');
  return $query->result_array();
 }


 function update_api($id_mesin, $data)
 {
  $this->db->where("id_mesin", $id_mesin);
  $this->db->update("mesin", $data);
 }


 function delete_single_user($id_mesin)
 {
  $this->db->where("id_mesin", $id_mesin);
  $this->db->delete("mesin");
  if($this->db->affected_rows() > 0)
  {
   return true;
  }
  else
  {
   return false;
  }
 }
}
