<?php
class Maintenance_model extends CI_Model
{
 function fetch_all()
 {
  $this->db->select ('M.nama_mesin, M.kode_mesin,P.id_mesin, P.id_perbaikan, P.permasalahan, P.penyelesaian, P.waktu_mulai, P.waktu_akhir, P.status');
  $this->db->from('mesin as M');
  $this->db->join ('perbaikan_mesin as P',' P.id_mesin = M.id_mesin');
   $this->db->group_by('P.id_perbaikan');
  return $this->db->get('perbaikan_mesin');
 }

 function getMesinlDDL()
 {

     $this->db->select('M.id_mesin, M.nama_mesin, M.kode_mesin ');
     $this->db->from('mesin as M');
     $status = "Aktif";
     $array = array('M.status' => $status);
     $this->db->where($array);
     $this->db->group_by('M.id_mesin');
     $query = $this->db->get('perbaikan_mesin');
     return $query->result_array();
  }

 function insert_api($id_mesin,$status,$data)
 {
  // $this->db->insert('perbaikan_mesin', $data);
  // if($this->db->affected_rows() > 0)
  // {
  //  return true;
  // }
  // else
  // {
  //  return false;
  // }
  if($status == "Menunggu Perbaikan"){
  $this->db->where('id_mesin',$id_mesin);
  $where = '(status="Menunggu Perbaikan" or status = "Sedang diperbaiki")';

  $this->db->where($where);
  $q = $this->db->get('perbaikan_mesin');

  if($q->num_rows() < 1 ){
    $this->db->insert('perbaikan_mesin', $data);
    if($this->db->affected_rows() > 0)
    {
     return true;
   }else{
     return false;
   }
  }
  }
  else{
     return false;
  }
 }

 function fetch_single_user($id_perbaikan)
 {
  $this->db->where("id_perbaikan", $id_perbaikan);
  $query = $this->db->get('perbaikan_mesin');
  return $query->result_array();
 }


 function update_api($id_perbaikan, $data)
 {
  $this->db->where("id_perbaikan", $id_perbaikan);
  $this->db->update("perbaikan_mesin", $data);
 }


 function delete_single_user($id_perbaikan)
 {
  $status = "Rusak";
  $array = array('id_perbaikan' => $id_perbaikan, 'status' => $status);
  $this->db->where($array);
  $this->db->delete("perbaikan_mesin");
  if($this->db->affected_rows() > 0)
  {
   return true;
  }
  else
  {
   return false;
  }
 }
}
