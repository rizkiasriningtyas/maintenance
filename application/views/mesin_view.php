<html>
<head>
    <title>MAINTENANCE MESIN</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <br/>
      <center>
        <li style="color:red;font-size:30px;" class="nav justify-content-center">MAINTENANCE MESIN </li>
      </center>
    <br/>
  </div>
</nav>

<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav_item">
        <a class="nav-link active" href="<?php echo site_url('./Mesin_controller')?>">Mesin</a>
      </li>
      <li class="nav-item">
      <a href="<?php echo site_url('./Maintenance_controller')?>">Maintenance</a>
      </li>
    </ul>
  </div>
</div>


<body>
    <div class="container">
        <br />
        <h3 align="center">KELOLA MESIN</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#aecfcb">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">DATA MESIN</h3>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" id="add_button" class="btn btn-dark">Tambah</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <span id="success_message"></span>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Nama Mesin</th>
                            <th>Kode Mesin</th>
                            <th>Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Mesin</h4>
                </div>
                <div class="modal-body">
                    <label>Nama Mesin</label>
                    <input type="text" name="nama_mesin" id="nama_mesin" class="form-control" />
                    <span id="nama_mesin_error" class="text-danger"></span>
                    <br />
                    <label>Kode Mesin</label>
                    <input type="text" name="kode_mesin" id="kode_mesin" class="form-control" />
                    <span id="kode_mesin_error" class="text-danger"></span>
                    <br />
                    <label id="lblStatus">Status</label>
                    <select name="status" id="status" class="form-control form-control-user" required>
                        <option value="Aktif" name="status">Aktif</option>
                        <option value="Tidak Aktif" name="status">Tidak Aktif</option>
                    </select>
                    <span id="status_error" class="text-danger"></span>
                    <br />
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_mesin" id="id_mesin" />
                    <input type="hidden" name="data_action" id="data_action" value="Insert" />
                    <input type="submit" name="action" id="action" class="btn btn-primary" value="Add" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

    function fetch_data()
    {
        $.ajax({
            url:"<?php echo base_url(); ?>Mesin_controller/action",
            method:"POST",
            data:{data_action:'fetch_all'},
            success:function(data)
            {
                $('tbody').html(data);
            }
        });
    }

    fetch_data();

    $('#add_button').click(function(){
      $('#status').hide();
      $('#lblStatus').hide();
      $('#nama_mesin_error').html('');
      $('#kode_mesin_error').html('');
        $('#user_form')[0].reset();
        $('.modal-title').text("Tambah Data Mesin");
        $('#action').val('Add');
        $('#data_action').val("Insert");
        $('#userModal').modal('show');
    });

    $(document).on('submit', '#user_form', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url() . 'Mesin_controller/action' ?>",
            method:"POST",
            data:$(this).serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.success)
                {
                    $('#user_form')[0].reset();
                    $('#userModal').modal('hide');
                    fetch_data();
                    if($('#data_action').val() == "Insert")
                    {
                      if(data.success)
                      {
                        $('#success_message').html('<div class="alert alert-success">Data berhasil ditambahkan</div>');
                      }
                    }
                }
                else if(data.error)
                {
                  $('#status').hide();
                  $('#lblStatus').hide();
                  $('#nama_mesin_error').html(data.nama_mesin_error);
                  $('#kode_mesin_error').html(data.kode_mesin_error);
                }
                else{
                  $('#user_form')[0].reset();
                  $('#userModal').modal('hide');
                  fetch_data();
                    $('#success_message').html('<div class="alert alert-danger" >Tidak dapat menambahkan data,Kode Mesin telah digunakan</div>');

                }
            }
        })
    });

    $(document).on('click', '.edit', function(){
        var id_mesin = $(this).attr('id_mesin');
        $.ajax({
            url:"<?php echo base_url(); ?>Mesin_controller/action",
            method:"POST",
            data:{id_mesin:id_mesin, data_action:'fetch_single'},
            dataType:"json",
            success:function(data)
            {
                $('#nama_mesin_error').html('');
                $('#kode_mesin_error').html('');
               $('#status').show();
               $('#lblStatus').show();
                $('#userModal').modal('show');
                $('#nama_mesin').val(data.nama_mesin);
                $('#kode_mesin').val(data.kode_mesin);
                $('#status').val(data.status);
                $('.modal-title').text('Ubah Mesin');
                $('#id_mesin').val(id_mesin);
                $('#action').val('Edit');
                $('#data_action').val('Edit');
            }
        })
    });

    $(document).on('click', '.delete', function(){
        var id_mesin = $(this).attr('id_mesin');
        if(confirm("Apakah kamu yakin menghapus data ini?"))
        {
            $.ajax({
                url:"<?php echo base_url(); ?>Mesin_controller/action",
                method:"POST",
                data:{id_mesin:id_mesin, data_action:'Delete'},
                dataType:"JSON",
                success:function(data)
                {
                    if(data.success)
                    {
                        $('#success_message').html('<div class="alert alert-success">Data Berhasil dihapus</div>');
                        fetch_data();
                    }
                    else{
                        $('#success_message').html('<div class="alert alert-danger">Tidak dapat menghapus data karena mesin sedang dalam proses perbaikan</div>');
                    }
                }
            })
        }
    });

});
</script>
