<?php ob_start(); ?>
        <?php
            $connect = mysqli_connect("localhost", "root", "","tokoku");
            mysqli_select_db($connect,"tokoku");
        ?>

                        <div class="card-header">
                            <h4 class="card-title">Data Barang</h4>
                            <button type="button" id="btnTambah" class="btn btn-primary pull-right" data-toggle="modal" data-target="#tambahData">Tambah Data</button>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table id="add-row" class="table table-striped zero-configuration" >
                                        <thead>
                                            <tr>
                                                <!-- <th>ID Barang</th> -->
                                                <th>Kode Barang</th>
                                                <th>Nama Barang</th>
                                                <th>Harga Beli</th>
                                                <th>Harga Jual</th>
                                                <th>Stok</th>
                                                <th>Satuan</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($barang as $u){
                                                // $jenis = mysqli_fetch_array(mysqli_query($connect,"select namaJenisTanaman from jenisTanaman where idJenisTanaman = ".$u->jenisTanaman));
                                                // $namaCreated = mysqli_fetch_array(mysqli_query($connect,"select nama from user where username = '".$u->createdBy."'"));
                                                // $namaModified = mysqli_fetch_array(mysqli_query($connect,"select nama from user where username = '".$u->modifiedBy."'"));
                                            ?>
                                                <tr>
                                                    <!-- <td><?php echo $u->id?></td> -->
                                                    <td><?php echo $u->kode_barang?></td>
                                                    <td><?php echo $u->nama_barang?></td>
                                                    <!-- <td><?php echo $jenis[0]?></td> -->
                                                    <td><?php echo 'Rp '.number_format($u->harga_beli,2, ",", ".") ?></td>
                                                    <td><?php echo 'Rp '.number_format($u->harga_jual,2, ",", ".") ?></td>
                                                    <td><?php echo $u->stok?></td>
                                                    <td><?php echo $u->satuan?></td>
                                                    <td>
                                                        <?php if($u->status == 0){
                                                          echo "Tidak Aktif";
                                                        }else{
                                                          echo "Aktif";
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="fa fa-pencil color-muted editbtn"></a>

                                                        <?php if($u->status == 0){
                                                          ?>
                                                          <a href="javascript:void(0);" style="margin-left: 30px;" class="delete_record fa fa-check color-danger aktifkanbtn"></a>
                                                          <?php
                                                        }else{
                                                          ?>
                                                          <a href="javascript:void(0);" style="margin-left: 30px;" class="delete_record fa fa-close color-danger hapusbtn"></a>
                                                          <?php
                                                        } ?>

                                                        <a href="javascript:void(0);" class="detailmdl" style="margin-left: 25px;">Lihat Detail</a>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- MODAL -->

<!-- DETAIL-->
<div class="modal fade" id="detailData" tabindex="-1" role="dialog" aria-labelledby="detailData" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">


                        <br/>
                        <label>Dibuat oleh</label>
                        <input type="text" id="creaby" class="form-control form-control-user" readonly="true" style="width: 600px" />
                        <br/>
                        <label>Dibuat Tanggal</label>
                        <input type="text" id="creadate" class="form-control form-control-user" readonly="true" style="width: 600px" />
                        <br/>
                        <label>Dimodifikasi oleh</label>
                        <input type="text" id="modiby" class="form-control form-control-user" readonly="true" style="width: 600px" />
                        <br/>
                        <label>Dimodifikasi Tanggal</label>
                        <input type="text" id="modidate" class="form-control form-control-user" readonly="true" style="width: 600px" />

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
              </div>
            </div>
    </div>
</div>


<!-- TAMBAH BARANG -->
<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="tambahData" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document" >
        <form action="<?php echo base_url('barang_lihat/save')?>" method="POST" enctype="multipart/form-data">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Tambah Data Tanaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                     <form role="form" action="<?php echo site_url('barang_lihat/save')?>" method="post" autocomplete="off" enctype="multipart/form-data">
                                     <div class="form-group row align-items-center">
                                         <label class="col-sm-3 col-form-label text-label">Kode Barang</label>
                                         <div class="col-sm-9">
                                             <div class="input-group">
                                                  <input type="text" id="kode_barang" name="kode_barang" class="form-control form-control-user" required></input>
                                              </div>
                                         </div>
                                     </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                     <input type="text" id="nama" name="nama" class="form-control form-control-user" required></input>
                                                 </div>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Stok</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <input type="number" id="stok" name="stok" class="form-control form-control-user" required></input>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                           <label class="col-sm-3 col-form-label text-label">Satuan</label>
                                           <div class="col-sm-9">
                                               <div class="input-group">
                                               <select name="satuan" id="satuan" class="form-control form-control-user" required>
                                                   <option value="Kg" name="satuan">Kg</option>
                                                   <option value="Buah" name="satuan">Buah</option>
                                                   <option value="Pack" name="satuan">Pack</option>
                                               </select>
                                               </div>

                                           </div>
                                       </div>
                                         <!-- <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <select name="jenis" class="form-control form-control-user" id="jenis" style="width: 180px;">
                                                                    <?php
                                                                        $res = mysqli_query($connect, "SELECT * FROM jenistanaman");
                                                                        while ($row=mysqli_fetch_array($res))
                                                                        {
                                                                    ?>
                                                                        <option value='<?=$row[0]?>'><?php echo $row['namaJenisTanaman']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                </select>
                                                </div>
                                            </div>
                                        </div> -->
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Harga Beli (Rp.)</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <input type="number" id="harga_beli" name="harga_beli" class="form-control form-control-user" required></input>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                           <label class="col-sm-3 col-form-label text-label">Harga Jual (Rp.)</label>
                                           <div class="col-sm-9">
                                               <div class="input-group">
                                                <input type="number" id="harga_jual" name="harga_jual" class="form-control form-control-user" required></input>
                                               </div>

                                           </div>
                                       </div>

                            </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button ID="submit" name="submit" class="btn btn-primary shadow-sm" Text="Create">Tambah</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- end tambah karyawan-->

<!-- EDIT BARANG -->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document" >
        <form action="<?php echo base_url('tanaman_lihat/edit')?>" method="POST" enctype="multipart/form-data">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Edit Data Tanaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="row match-height">
                        <div class="col-12">
                        <form role="form" action="<?php echo site_url('tanaman_lihat/edit')?>" method="post" autocomplete="off" enctype="multipart/form-data">
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">ID</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                     <input type="text" id="idU" name="idU" class="form-control form-control-user" readonly required></input>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                     <input type="text" id="namaU" name="namaU" class="form-control form-control-user" required></input>
                                                 </div>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Stok(buah)</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <input type="number" id="stokU" name="stokU" class="form-control form-control-user" required></input>
                                                </div>

                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <select name="jenisU" id="jenisU" class="form-control form-control-user" style="width: 180px;">
                                                                    <?php
                                                                        $res = mysqli_query($connect, "SELECT * FROM jenistanaman");
                                                                        while ($row=mysqli_fetch_array($res))
                                                                        {
                                                                    ?>
                                                                        <option value='<?=$row[0]?>'><?php echo $row['namaJenisTanaman']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Harga</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <input type="number" id="hargaU" name="hargaU" class="form-control form-control-user" required></input>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Gambar</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <input class="form-control form-control-user" type="file" name="fotoU" id="fotoU" />
                                                </div>

                                            </div>
                                        </div>
                                </form>
                        </div>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button ID="submit" name="updateData" class="btn btn-primary shadow-sm" Text="Edit">Ubah</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- end edit karyawan-->

<!-- HAPUS KARYAWAN -->
<div class="modal fade" id="hapusmodal" tabindex="-1" role="dialog" aria-labelledby="hapusmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?php echo base_url('barang_lihat/hapus')?>" method="POST">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Menonaktifkan Data Tanaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <input type="hidden" name="idD" id="idD" class="form-control">
                Yakin Menonaktifkan data ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="hapusdata" class="btn btn-primary">Nonaktifkan</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- end hapus karyawan-->

<div class="modal fade" id="aktifkanmodal" tabindex="-1" role="dialog" aria-labelledby="aktifkanmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?php echo base_url('barang_lihat/aktifkan')?>" method="POST">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Aktifkan Data Tanaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <input type="hidden" name="idA" id="idA" class="form-control">
                Yakin mengaktifkan data ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="aktifkandata" class="btn btn-primary">Aktifkan</button>
              </div>
            </div>
        </form>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.aktifkanbtn').on('click', function(){
                $('#aktifkanmodal').modal('show');
                $tr = $(this).closest('tr');
                var data = $tr.children("td").map(function(){
                    return $(this).text();
                }).get();
                console.log(data);

                $('#idA').val(data[0]);
            });
        });
</script>


<script>
        $(document).ready(function(){
            $('.detailmdl').on('click', function(){
                $('#detailData').modal('show');
                $tr = $(this).closest('tr');
                var data = $tr.children("td").map(function(){
                    return $(this).text();
                }).get();
                console.log(data);

                $('#creaby').val(data[7]);
                $('#creadate').val(data[8]);
                $('#modiby').val(data[9]);
                $('#modidate').val(data[10]);
            });
        });
</script>

 <script>
        $(document).ready(function(){
            $('.editbtn').on('click', function(){
                $('#editmodal').modal('show');
                $tr = $(this).closest('tr');
                var data = $tr.children("td").map(function(){
                    return $(this).text();
                }).get();
                console.log(data);

                $('#idU').val(data[0]);
                $('#namaU').val(data[1]);
                $('#stokU').val(data[2]);
                $('#hargaU').val(data[4]);
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.hapusbtn').on('click', function(){
                $('#hapusmodal').modal('show');
                $tr = $(this).closest('tr');
                var data = $tr.children("td").map(function(){
                    return $(this).text();
                }).get();
                console.log(data);

                $('#idD').val(data[0]);
            });
        });
</script>

        <script>
            $( document ).ready(function() {
                $( "#namaU" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
            $( document ).ready(function() {
                $( "#nama" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

<?php
$data = ob_get_clean();
include('master_page.php');
ob_flush();
?>
