<html>
<head>
    <title>MAINTENANCE MESIN</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <br/>
      <center>
        <li style="color:red;font-size:30px;" class="nav justify-content-center">MAINTENANCE MESIN </li>
      </center>
    <br/>
  </div>
</nav>

<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav_item">
        <a href="<?php echo site_url('./Mesin_controller')?>">Mesin</a>
      </li>
      <li class="nav-item">
      <a class="nav-link active"href="<?php echo site_url('./Maintenance_controller')?>">Maintenance</a>
      </li>
    </ul>
  </div>
</div>

<body>
    <div class="container">
        <br />
        <h3 align="center">MAINTENANCE</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#aecfcb">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">MAINTENANCE</h3>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" id="add_button" class="btn btn-dark">Tambah</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <span id="success_message"></span>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th hidden>ID Mesin</th>
                            <th>Nama Mesin</th>
                            <th>Kode Mesin</th>
                            <th>Permasalahan</th>
                            <th>Penyelesaian</th>
                            <th>Waktu Mulai</th>
                            <th>Waktu Selesai</th>
                            <th>Status</th>
                            <th>Aksi</th>
                            <!-- <th>Delete</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Data Maintenance</h4>
                </div>
                <div class="modal-body">
                    <label>Nama Mesin</label>
                    <select id="ddlMesin" name="id_mesin" class="form-control form-control-user" style="width: 100%;" required></select>
                    <span id="mesin_error" class="text-danger"></span>
                    <br />
                    <label>Permasalahan</label>
                    <input type="text" name="permasalahan" id="permasalahan" class="form-control" />
                    <span id="permasalahan_error" class="text-danger"></span>
                    <br />
                    <label>Penyelesaian</label>
                    <input type="text" name="penyelesaian" id="penyelesaian" class="form-control" />
                    <span id="penyelesaian_error" class="text-danger"></span>
                    <br />
                    <label id="lblStatus">Enter Status</label>
                    <select name="status" id="status" class="form-control form-control-user" required>
                        <option hidden value="Menunggu Perbaikan" name="status">Menunggu Perbaikan</option>
                        <option value="Sedang diperbaiki"  name="status">Sedang diperbaiki</option>
                        <option value="Selesai diperbaiki" name="status">Selesai diperbaiki</option>
                        <option value="Rusak" name="status">Rusak</option>
                    </select>
                    <span id="status_error" class="text-danger"></span>
                    <br />
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_perbaikan" id="id_perbaikan" />
                    <input type="hidden" name="waktu_mulai" id="waktu_mulai" />
                    <input type="hidden" name="data_action" id="data_action" value="Insert" />
                    <input type="submit" name="action" id="action" class="btn btn-primary" value="Add" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

  $.ajax({
    type: "GET",
    url: "<?php echo site_url('api/Api_Maintenance/ListMesin') ?>",
    data: "{}",
    dataType: 'html',
    success: function(data) {

      var s = '<option value="-1">-- Pilih Mesin --</option>';
      $.each(JSON.parse(data), function(key, val) {

          s += '<option value="' + val.id_mesin + '">' + val.nama_mesin + " " + val.kode_mesin + '</option>';
      });
       $("#ddlMesin").html("");
        $("#ddlMesin").html(s);
      $("#modalMesin").html("");

    },
});
    function fetch_data()
    {
        $.ajax({
            url:"<?php echo base_url(); ?>Maintenance_controller/action",
            method:"POST",
            data:{data_action:'fetch_all'},
            success:function(data)
            {
                $('tbody').html(data);
            }
        });
    }

    fetch_data();

    $('#add_button').click(function(){
        $('#status').hide();
        $('#lblStatus').hide();
        $('#mesin_error').html('');
        $('#permasalahan_error').html('');
        $('#penyelesaian_error').html('');
        $('#user_form')[0].reset();
        $('.modal-title').text("Tambah Data Maintenance");
        $('#action').val('Add');
        $('#data_action').val("Insert");
        $('#userModal').modal('show');
    });

    $('#data_action').click(function() {
      $('#status').show();
      $('#lblStatus').show();
    });

    $(document).on('submit', '#user_form', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url() . 'Maintenance_controller/action' ?>",
            method:"POST",
            data:$(this).serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.success)
                {
                    $('#user_form')[0].reset();
                    $('#userModal').modal('hide');
                    fetch_data();
                    if($('#data_action').val() == "Insert")
                    {
                        $('#success_message').html('<div class="alert alert-success">Data Berhasil ditambahkan</div>');
                    }
                }

                else if(data.error)
                {
                  $('#status').hide();
                  $('#lblStatus').hide();
                    $('#mesin_error').html(data.mesin_error);
                    $('#permasalahan_error').html(data.permasalahan_error);
                    $('#penyelesaian_error').html(data.penyelesaian_error);
                }
                else{
                  $('#user_form')[0].reset();
                  $('#userModal').modal('hide');
                  fetch_data();
                  $('#success_message').html('<div class="alert alert-danger" >Tidak dapat menambahkan data, Mesin yang sama sedang dalam maintenance</div>');
                }
            }
        })
    });

    $(document).on('click', '.edit', function(){
        var id_perbaikan = $(this).attr('id_perbaikan');
        $.ajax({
            url:"<?php echo base_url(); ?>Maintenance_controller/action",
            method:"POST",
            data:{id_perbaikan:id_perbaikan, data_action:'fetch_single'},
            dataType:"json",
            success:function(data)
            {
              $('#mesin_error').html('');
              $('#permasalahan_error').html('');
              $('#penyelesaian_error').html('');
                $('#status').show();
                $('#lblStatus').show();
                $('#userModal').modal('show');
                $('#ddlMesin').val(data.id_mesin);
                $('#permasalahan').val(data.permasalahan);
                $('#penyelesaian').val(data.penyelesaian);
                $('#status').val(data.status);
                $('.modal-title').text('Edit Maintenance');
                $('#id_perbaikan').val(id_perbaikan);
                $('#action').val('Edit');
                $('#data_action').val('Edit');

            }
        })
    });

    $(document).on('click', '.delete', function(){
        var id_perbaikan = $(this).attr('id_perbaikan');
        if(confirm("Apakah kamu yakin menghapus data ini?"))
        {
            $.ajax({
                url:"<?php echo base_url(); ?>Maintenance_controller/action",
                method:"POST",
                data:{id_perbaikan:id_perbaikan, data_action:'Delete'},
                dataType:"JSON",
                success:function(data)
                {
                    if(data.success)
                    {
                        $('#success_message').html('<div class="alert alert-success">Data Berhasil dihapus</div>');
                        fetch_data();
                    }
                    else{
                      $('#success_message').html('<div class="alert alert-success">Data yang bisa dihapus adalah yang berstatus rusak</div>');
                      fetch_data();
                    }
                }
            })
        }
    });

});
</script>
