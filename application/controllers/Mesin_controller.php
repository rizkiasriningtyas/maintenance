<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesin_controller extends CI_Controller {

 function index()
 {
  $this->load->view('mesin_view');
 }

 function action()
 {
  if($this->input->post('data_action'))
  {
   $data_action = $this->input->post('data_action');

   if($data_action == "Delete")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_mesin/delete";

    $form_data = array(
     'id_mesin'  => $this->input->post('id_mesin')
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;




   }

   if($data_action == "Edit")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_mesin/update";

    $form_data = array(
     'nama_mesin'  => $this->input->post('nama_mesin'),
     'kode_mesin'   => $this->input->post('kode_mesin'),
     'status'   => $this->input->post('status'),
     'id_mesin'    => $this->input->post('id_mesin'),
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;
   }

   if($data_action == "fetch_single")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_mesin/fetch_single";

    $form_data = array(
     'id_mesin'  => $this->input->post('id_mesin')
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;
   }

   if($data_action == "Insert")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_mesin/insert";

    $form_data = array(
     'nama_mesin'  => $this->input->post('nama_mesin'),
     'kode_mesin'  => $this->input->post('kode_mesin'),
     'status'      => 'Aktif',
     'id_mesin'    => $this->input->post('id_mesin')
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;


   }

   if($data_action == "fetch_all")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_mesin";

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    $result = json_decode($response);

    $output = '';

    if(count($result) > 0)
    {
     foreach($result as $row)
     {
      $output .= '
      <tr>
       <td>'.$row->nama_mesin.'</td>
       <td>'.$row->kode_mesin.'</td>
       <td>'.$row->status.'</td>
       <td align="center" >
       <button type="button" name="edit" id="editbtn" class="btn btn-warning edit" id_mesin="'.$row->id_mesin.'">Ubah</button>
      <button type="button" name="delete" class="btn btn-danger delete" id_mesin="'.$row->id_mesin.'">Hapus</button></td>
      </tr>

      ';
     }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }

    echo $output;
   }
  }
 }

}

?>
