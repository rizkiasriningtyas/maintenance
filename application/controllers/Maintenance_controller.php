<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance_controller extends CI_Controller {

 function index()
 {
  $this->load->view('maintenance_view');
 }


 function action()
 {
  if($this->input->post('data_action'))
  {
   $data_action = $this->input->post('data_action');

   if($data_action == "Delete")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_maintenance/delete";

    $form_data = array(
     'id_perbaikan'  => $this->input->post('id_perbaikan')
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;
   }

   if($data_action == "Edit")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_maintenance/update";
    $form_data = array(
     'id_mesin'       => $this->input->post('id_mesin'),
     'permasalahan'   =>$this->input->post('permasalahan'),
     'penyelesaian'   => $this->input->post('penyelesaian'),
     'status'         => $this->input->post('status'),
     'waktu_akhir'    => date("Y-m-d H:i:s"),

     'id_perbaikan'   => $this->input->post('id_perbaikan'),
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;

   }

   if($data_action == "fetch_single")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_maintenance/fetch_single";

    $form_data = array(
     'id_perbaikan'  => $this->input->post('id_perbaikan')
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;
   }

   if($data_action == "Insert")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_maintenance/insert";
    date_default_timezone_set("Asia/Bangkok");

    $form_data = array(
      'id_mesin'       => $this->input->post('id_mesin'),
      'permasalahan'    => $this->input->post('permasalahan'),
      'penyelesaian'   => $this->input->post('penyelesaian'),
      'waktu_mulai'    => date("Y-m-d H:i:s"),
      'waktu_akhir'    => 'NULL',
      'status'         => 'Menunggu Perbaikan',
      'id_perbaikan'   => $this->input->post('id_perbaikan'),
    );

    $client = curl_init($api_url);

    curl_setopt($client, CURLOPT_POST, true);

    curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($client);

    curl_close($client);

    echo $response;

   }


   if($data_action == "fetch_all")
   {
    $api_url = "http://localhost:8080/maintenance/api/api_maintenance";
    $client = curl_init($api_url);
    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($client);
    curl_close($client);
    $result = json_decode($response);
    $output = '';

    if(count($result) > 0)
    {
     foreach($result as $row)
     {
      $output .= '
      <tr>
      <td hidden>'.$row->id_mesin.'</td>
      <td>'.$row->nama_mesin.'</td>
      <td>'.$row->kode_mesin.'</td>
      <td>'.$row->permasalahan.'</td>
      <td>'.$row->penyelesaian.'</td>
      <td>'.$row->waktu_mulai.'</td>
      <td>'.$row->waktu_akhir.'</td>
      <td>'.$row->status.'</td>
        <td><button type="button" id="editbtn" name="edit" class="btn btn-warning btn-xs edit" id_perbaikan="'.$row->id_perbaikan.'">Ubah</button>
        <button type="button" name="delete" class="btn btn-danger btn-xs delete" id_perbaikan="'.$row->id_perbaikan.'">Hapus</button></td>
      </tr>

      ';
     }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }

    echo $output;
   }
  }
 }

}

?>
