<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Mesin extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('Mesin_model');
  $this->load->library('form_validation');
 }

 function index()
 {
  $data = $this->Mesin_model->fetch_all();
  echo json_encode($data->result_array());
 }

 function insert()
 {
  $this->form_validation->set_rules("nama_mesin", "Nama Mesin", "required");
  $this->form_validation->set_rules("kode_mesin", "Kode Mesin", "required");
  $this->form_validation->set_rules("status", "Status", "required");
  $array = array();
  if($this->form_validation->run())
  {
   $data = array(
    'nama_mesin' => trim($this->input->post('nama_mesin')),
    'kode_mesin'  => trim($this->input->post('kode_mesin')),
    'status'  => trim($this->input->post('status'))
   );
   // $this->Mesin_model->insert_api($this->input->post('kode_mesin'),$data);
   // $array = array(
   //  'success'  => true
   // );
     if($this->Mesin_model->insert_api($this->input->post('kode_mesin'),$data))
     {
      $array = array(
       'success' => true
      );
    }
    else
    {
     $array = array(
      'succes' => false
     );
    }
  }
  else
  {
   $array = array(
    'error'    => true,
    'nama_mesin_error' => form_error('nama_mesin'),
    'kode_mesin_error' => form_error('kode_mesin')
   );
  }
  echo json_encode($array, true);
 }

 function fetch_single()
 {
  if($this->input->post('id_mesin'))
  {
   $data = $this->Mesin_model->fetch_single_user($this->input->post('id_mesin'));
   foreach($data as $row)
   {
    $output['nama_mesin'] = $row["nama_mesin"];
    $output['kode_mesin'] = $row["kode_mesin"];
    $output['status'] = $row["status"];
   }
   echo json_encode($output);
  }
 }

 function update()
 {
   $this->form_validation->set_rules("nama_mesin", "Nama Mesin", "required");
   $this->form_validation->set_rules("kode_mesin", "Kode Mesin", "required");
   $this->form_validation->set_rules("status", "Status", "required");
  $array = array();
  if($this->form_validation->run())
  {
   $data = array(
     'nama_mesin' => trim($this->input->post('nama_mesin')),
     'kode_mesin'  => trim($this->input->post('kode_mesin')),
     'status'  => trim($this->input->post('status'))
   );
   $this->Mesin_model->update_api($this->input->post('id_mesin'), $data);
   $array = array(
    'success'  => true
   );
  }
  else
  {
   $array = array(
     'error'    => true,
     'nama_mesin_error' => form_error('nama_mesin'),
     'kode_mesin_error' => form_error('kode_mesin'),
     'status' => form_error('status')
   );
  }
  echo json_encode($array, true);
 }

 function delete()
 {
  if($this->input->post('id_mesin'))
  {
   if($this->Mesin_model->delete_single_user($this->input->post('id_mesin')))
   {
    $array = array(
     'success' => true
    );
   }
   else
   {
    $array = array(
     'error' => true
    );
   }
   echo json_encode($array);
  }
 }

}
