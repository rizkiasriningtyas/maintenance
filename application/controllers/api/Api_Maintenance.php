<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Maintenance extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('Maintenance_model');
  $this->load->library('form_validation');
 }

 public function ListMesin()
 {
    $data = $this->Maintenance_model->getMesinlDDL();
    echo json_encode($data);
 }

 function index()
 {
  $data = $this->Maintenance_model->fetch_all();
  echo json_encode($data->result_array());
 }

 function insert()
 {
  $this->form_validation->set_rules("id_mesin", "Mesin", "required");
  $this->form_validation->set_rules("permasalahan", "Permasalahan", "required");
  $this->form_validation->set_rules("penyelesaian", "Penyelesaian", "required");
  $array = array();
  if($this->form_validation->run())
  {
   $data = array(
    'id_mesin' => trim($this->input->post('id_mesin')),
    'permasalahan'  => trim($this->input->post('permasalahan')),
    'penyelesaian' => trim($this->input->post('penyelesaian')),
    'waktu_mulai'  => trim($this->input->post('waktu_mulai')),
    'waktu_akhir'  => 'NULL',
    'status'  => trim($this->input->post('status'))
   );
   // $this->Maintenance_model->insert_api($data);
   // $array = array(
   //  'success'  => true
   // );
   if($this->Maintenance_model->insert_api($this->input->post('id_mesin'),$this->input->post('status'),$data))
   {
    $array = array(
     'success' => true
    );
  }
  else
  {
   $array = array(
    'succes' => false
   );
  }
}
else
{
  $array = array(
    'error'    => true,
    'mesin_error' => form_error('id_mesin'),
    'permasalahan_error' => form_error('permasalahan'),
    'penyelesaian_error' => form_error('penyelesaian'),
    'waktu_mulai' => form_error('waktu_mulai'),
    'waktu_akhir' => form_error('waktu_akhir'),
    'status_error' => form_error('status')
  );
}
echo json_encode($array, true);
}

 function fetch_single()
 {
  if($this->input->post('id_perbaikan'))
  {
   $data = $this->Maintenance_model->fetch_single_user($this->input->post('id_perbaikan'));
   foreach($data as $row)
   {
    $output['id_mesin'] = $row["id_mesin"];
    $output['permasalahan'] = $row["permasalahan"];
    $output['penyelesaian'] = $row["penyelesaian"];
    $output['waktu_mulai'] = $row["waktu_mulai"];
    $output['waktu_akhir'] = $row["waktu_akhir"];
    $output['status'] = $row["status"];
   }
   echo json_encode($output);
  }
 }

 function update()
 {
   $this->form_validation->set_rules("id_mesin", "Mesin", "required");
   $this->form_validation->set_rules("permasalahan", "Permasalahan", "required");
   $this->form_validation->set_rules("penyelesaian", "Penyelesaian", "required");
   date_default_timezone_set("Asia/Bangkok");
  $array = array();
  if($this->form_validation->run())
  {
   $data = array(
     'id_mesin' => trim($this->input->post('id_mesin')),
     'permasalahan'  => trim($this->input->post('permasalahan')),
     'penyelesaian' => trim($this->input->post('penyelesaian')),
     // 'waktu_mulai' => trim($this->input->post('waktu_mulai')),
     'waktu_akhir'  => $this->input->post('status') == 'Sedang diperbaiki' ? 'NULL' : trim(date("Y-m-d H:i:s")),
     'status'  => trim($this->input->post('status'))
   );
   $this->Maintenance_model->update_api($this->input->post('id_perbaikan'), $data);
   $array = array(
    'success'  => true
   );
  }
  else
  {
   $array = array(
     'error'    => true,
     'mesin_error' => form_error('id_mesin'),
     'permasalahan_error' => form_error('permasalahan'),
     'penyelesaian_error' => form_error('penyelesaian'),
     'waktu_mulai' => form_error('waktu_mulai'),
     'waktu_akhir' => form_error('waktu_akhir'),
     'status_error' => form_error('status')
   );
  }
  echo json_encode($array, true);
 }

 function delete()
 {
  if($this->input->post('id_perbaikan'))
  {
   if($this->Maintenance_model->delete_single_user($this->input->post('id_perbaikan')))
   {
    $array = array(
     'success' => true
    );
   }
   else
   {
    $array = array(
     'error' => true
    );
   }
   echo json_encode($array);
  }
 }

}
