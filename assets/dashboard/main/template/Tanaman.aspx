﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main/template/MasterPage - Admin.master" AutoEventWireup="true" CodeFile="Tanaman.aspx.cs" Inherits="main_template_Tanaman" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Tanaman
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
    <form id="form1" runat="server">
    <div id="content" style="margin-left:30px;">
          <asp:ScriptManager
            id="sm1"
            runat="server"
            />
    <div id="view" runat="server">
    <asp:Label Text="Lihat Tanaman" runat="server"></asp:Label>
     
    <br/><br/>

        <asp:GridView
            id="grdData"
            AllowPaging="true"
            AllowSorting="true"
            OnPageIndexChanging="grdData_PageIndexChanging"
            OnRowCommand="grdData_RowCommand"
            OnRowDeleting="grdData_RowDeleting"
            OnSorting="grdData_Sorting"
            DataKeyNames="idTanaman" 
            AutoGenerateColumns="false"
            GridLines="None"
            CssClass="list"
            Runat="server" Width="1000px" Height="80px">
            <Columns>
            
            <asp:TemplateField>
             <ItemTemplate>
                
                <asp:Label style="margin-right:20px; margin-left:10px;" runat="server" ID="lblID" Text='<%#Eval("idTanaman") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="namaTanaman" HeaderText="Nama Tanaman" SortExpression="namaTanaman"/>
            <asp:BoundField DataField="stokTanaman" HeaderText="Stok" SortExpression="stokTanaman"/>
            <asp:BoundField DataField="jenisTanaman" HeaderText="Jenis Tanaman" SortExpression="jenisTanaman"/>
            <asp:BoundField DataField="hargaTanaman" HeaderText="Harga" SortExpression="hargaTanaman"/>
            <asp:BoundField DataField="statusTanaman" HeaderText="Status" SortExpression="statusTanaman"/>

            <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="linkEdit" Text="Edit" runat="server" CommandName="Ubah" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'></asp:LinkButton>
                &nbsp;|&nbsp;
                <span onclick="return confirm('Are you sure you want to delete this record?')">
                <asp:LinkButton ID="linkHapus" Text="Hapus" runat="server" CommandName="Delete"></asp:LinkButton>
                </span>
            </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br /><br />
        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Tambah" runat="server" style="margin-left:10px"></asp:Button>
        </div>

    <div id="create" runat="server">
         <div class="col-xl-6">

                                <h4 class="card-title mb-4">Tambah</h4>
                                <div class="basic-form">
                                    <form>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtNama"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator3"
                                                                    ControlToValidate="txtNama"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />

                                                                <asp:RegularExpressionValidator
                                                                    ID="RegularExpressionValidator3"
                                                                    runat="server"
                                                                    ErrorMessage="Only characters allowed"
                                                                    ControlToValidate="txtNama"
                                                                    ValidationExpression="([A-Za-z])+( [A-Za-z]+)*" >
                                                                </asp:RegularExpressionValidator>
                                               
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Stok</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtStok"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator8"
                                                                    ControlToValidate="txtStok"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator5" 
                                                                    runat="server"
                                                                    ControlToValidate="txtStok"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:DropDownList
                                                    id="ddl_Jenis"
                                                    class="form-control"
                                                    Runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Harga</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtHarga"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator1"
                                                                    ControlToValidate="txtHarga"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator1" 
                                                                    runat="server"
                                                                    ControlToValidate="txtHarga"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

        <br/><br/>
            <asp:Button ID="btnCreate" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Create" runat="server" OnClick="btnCreate_Click"></asp:Button>

    </div>









        <div id="update" runat="server">
                <div class="col-xl-6">

                                <h4 class="card-title mb-4">Update</h4>
                                <div class="basic-form">
                                    <form>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">ID Tanaman</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtID"
                                                    class="form-control form-control-user"
                                                    ReadOnly="true"
                                                    Runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtNamaU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator2"
                                                                    ControlToValidate="txtNamaU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />

                                                                <asp:RegularExpressionValidator
                                                                    ID="RegularExpressionValidator2"
                                                                    runat="server"
                                                                    ErrorMessage="Only characters allowed"
                                                                    ControlToValidate="txtNamaU"
                                                                    ValidationExpression="([A-Za-z])+( [A-Za-z]+)*" >
                                                                </asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Stok</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtStokU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator4"
                                                                    ControlToValidate="txtStokU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator4" 
                                                                    runat="server"
                                                                    ControlToValidate="txtStokU"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:DropDownList
                                                    id="ddl_JenisU"
                                                    class="form-control"
                                                    Runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Harga</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtHargaU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator5"
                                                                    ControlToValidate="txtHargaU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator6" 
                                                                    runat="server"
                                                                    ControlToValidate="txtHargaU"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Status</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:RadioButtonList Style="margin-top:10px; margin-bottom:10px" ID="rbStatusU" runat="server" Text='<%# Bind("statusTanaman") %>'>
                                                    <asp:ListItem value="1" Selected="True">Aktif</asp:ListItem>
                                                    <asp:ListItem Value="0" >Tidak Aktif</asp:ListItem>
                                                </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
        <br/><br/>
            <asp:Button ID="btnUpdate" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Update" runat="server" OnClick="btnUpdate_Click"></asp:Button>
            
    </div>

    





    <br/><br/>

    </div>
    </form>
</asp:Content>

