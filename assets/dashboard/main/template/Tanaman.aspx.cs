﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class main_template_Tanaman : System.Web.UI.Page
{
    SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["Greenivy"].ConnectionString);
    DataSet ds = new DataSet();

    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            loadData();
            update.Visible = false;
            create.Visible = false;
            view.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            SqlConnection sqlconn = new SqlConnection("Data Source=.;Initial Catalog=Greenivy;Persist Security Info=True;User ID=vai;Password=gwinget123");
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("Select * FROM jenisTanaman Order by namaJenisTanaman");
            DataTable dtLanguages = new DataTable();

            da.SelectCommand = cmd;
            da.SelectCommand.Connection = sqlconn;
            da.Fill(dtLanguages);
            ddl_Jenis.DataSource = dtLanguages;
            ddl_Jenis.DataTextField = "namaJenisTanaman";
            ddl_Jenis.DataValueField = "idJenisTanaman";
            ddl_Jenis.DataBind();

            da.SelectCommand = cmd;
            da.SelectCommand.Connection = sqlconn;
            da.Fill(dtLanguages);
            ddl_JenisU.DataSource = dtLanguages;
            ddl_JenisU.DataTextField = "namaJenisTanaman";
            ddl_JenisU.DataValueField = "idJenisTanaman";
            ddl_JenisU.DataBind();


        }

    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    public DataSet loadData()
    {
        SqlCommand com = new SqlCommand();
        com.Connection = connect;
        com.CommandText = "sp_viewTanaman";
        com.CommandType = CommandType.StoredProcedure;
        //com.Parameters.AddWithValue("@NamaMenu", txtSearch.Text);
        SqlDataAdapter adapt = new SqlDataAdapter(com);
        adapt.Fill(ds);
        grdData.DataSource = ds;
        grdData.DataBind();
        return ds;
    }

    public void SortGridView(string sortExpression, string direction)
    {
        DataTable dt = loadData().Tables[0];

        DataView dv = new DataView(dt);
        dv.Sort = sortExpression + direction;

        grdData.DataSource = dv;
        grdData.DataBind();
    }


    protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdData.PageIndex = e.NewPageIndex;
        loadData();
    }

    protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Ubah")
        {
            String id = grdData.DataKeys[Convert.ToInt32(e.CommandArgument.ToString())].Value.ToString();
            txtID.Text = id;
            txtNamaU.Text = grdData.Rows[Convert.ToInt32(e.CommandArgument.ToString())].Cells[1].Text;
            txtStokU.Text = grdData.Rows[Convert.ToInt32(e.CommandArgument.ToString())].Cells[2].Text;
            txtHargaU.Text = grdData.Rows[Convert.ToInt32(e.CommandArgument.ToString())].Cells[4].Text;

            update.Visible = true;
            create.Visible = false;
            view.Visible = false;
        }
    }

    protected void grdData_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            SortGridView(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            SortGridView(sortExpression, ASCENDING);
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        SqlCommand com = new SqlCommand();
        com.Connection = connect;
        com.CommandText = "sp_createTanaman";
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.AddWithValue("@namaTanaman", txtNama.Text);
        com.Parameters.AddWithValue("@stokTanaman", txtStok.Text);
        com.Parameters.AddWithValue("@jenisTanaman", ddl_Jenis.Text);
        com.Parameters.AddWithValue("@hargaTanaman", txtHarga.Text);
        com.Parameters.AddWithValue("@statusTanaman", "1");

        com.Parameters.AddWithValue("@createdBy", "");
        com.Parameters.AddWithValue("@createdDate", DateTime.Today);

        connect.Open();
        int result = com.ExecuteNonQuery();
        connect.Close();
        loadData();
        update.Visible = false;
        create.Visible = false;
        view.Visible = true;
    }



    protected void btnAdd_Click(object sender, EventArgs e)
    {
        update.Visible = false;
        create.Visible = true;
        view.Visible = false;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        SqlCommand com = new SqlCommand();
        com.Connection = connect;
        com.CommandText = "sp_updateTanaman";
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.AddWithValue("@idTanaman", txtID.Text);
        com.Parameters.AddWithValue("@namaTanaman", txtNamaU.Text);
        com.Parameters.AddWithValue("@stokTanaman", txtStokU.Text);
        com.Parameters.AddWithValue("@jenisTanaman", ddl_JenisU.Text);
        com.Parameters.AddWithValue("@hargaTanaman", txtHargaU.Text);
        com.Parameters.AddWithValue("@statusTanaman", rbStatusU.Text);

        com.Parameters.AddWithValue("@modifiedBy", "");
        com.Parameters.AddWithValue("@modifiedDate", DateTime.Today);


        connect.Open();
        int result = Convert.ToInt32(com.ExecuteNonQuery());
        connect.Close();
        loadData();
        update.Visible = false;
        create.Visible = false;
        view.Visible = true;
    }

    protected void grdData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SqlCommand com = new SqlCommand();
        com.Connection = connect;
        Label id = (Label)grdData.Rows[e.RowIndex].FindControl("lblID");
        com.CommandText = "sp_deleteTanaman";
        com.Parameters.AddWithValue("@idTanaman", Convert.ToInt32(id.Text));
        com.CommandType = CommandType.StoredProcedure;
        connect.Open();
        int result = Convert.ToInt32(com.ExecuteNonQuery());
        connect.Close();
        if (result > 0)
        {
            //lblMessage.Text = "Record Deleted Successfully";
            //lblMessage.ForeColor = System.Drawing.Color.Green;
            grdData.EditIndex = -1;
            loadData();
        }
        else
        {
            //lblMessage.Text = "Failed";
            //lblMessage.ForeColor = System.Drawing.Color.Red;
            loadData();
        }
    }

}