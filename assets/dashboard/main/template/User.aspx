﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main/template/MasterPage - Admin.master" AutoEventWireup="true" CodeFile="User.aspx.cs" Inherits="main_template_User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    User
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
           <form id="form1" runat="server">
    <div id="content" style="margin-left:30px;">
          <asp:ScriptManager
            id="sm1"
            runat="server"
            />
    <div id="view" runat="server">
    <asp:Label Text="Lihat User" runat="server"></asp:Label>
     
    <br/><br/>

        <asp:GridView
            id="grdData"
            AllowPaging="true"
            AllowSorting="true"
            OnPageIndexChanging="grdData_PageIndexChanging"
            OnRowCommand="grdData_RowCommand"
            OnRowDeleting="grdData_RowDeleting"
            OnSorting="grdData_Sorting"
            DataKeyNames="username" 
            AutoGenerateColumns="false"
            GridLines="None"
            CssClass="list"
            Runat="server" Width="1000px" Height="80px">
            <Columns>
            
            <asp:TemplateField>
             <ItemTemplate>
                
                <asp:Label style="margin-right:20px; margin-left:10px;" runat="server" ID="lblID" Text='<%#Eval("username") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="nama" HeaderText="Nama" SortExpression="nama"/>
            <asp:BoundField DataField="alamat" HeaderText="Alamat" SortExpression="alamat"/>
            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email"/>
            <asp:BoundField DataField="telepon" HeaderText="Telepon" SortExpression="telepon"/>
            <asp:BoundField DataField="jenisKelamin" HeaderText="Jenis Kelamin" SortExpression="jenisKelamin"/>
            <asp:BoundField DataField="role" HeaderText="Role" SortExpression="role"/>
            <asp:BoundField DataField="password" HeaderText="Password" SortExpression="password"/>
            <asp:BoundField DataField="statusUser" HeaderText="Status" SortExpression="statusUser"/>

            <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="linkEdit" Text="Edit" runat="server" CommandName="Ubah" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'></asp:LinkButton>
                &nbsp;|&nbsp;
                <span onclick="return confirm('Are you sure you want to delete this record?')">
                <asp:LinkButton ID="linkHapus" Text="Hapus" runat="server" CommandName="Delete"></asp:LinkButton>
                </span>
            </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br /><br />
        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Tambah" runat="server" style="margin-left:10px"></asp:Button>
        </div>

    <div id="create" runat="server">
         <div class="col-xl-6">

                                <h4 class="card-title mb-4">Tambah</h4>
                                <div class="basic-form">
                                    <form>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Username</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtUsername"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator6"
                                                                    ControlToValidate="txtUsername"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Password</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtPassword"
                                                    class="form-control form-control-user"
                                                    TextMode="Password"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator1"
                                                                    ControlToValidate="txtPassword"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Confirm Password</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtConfirmPassword"
                                                    class="form-control form-control-user"
                                                    TextMode="Password"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator11"
                                                                    ControlToValidate="txtConfirmPassword"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                                     ControlToValidate="txtConfirmPassword"
                                                                     CssClass="ValidationError"
                                                                     ControlToCompare="txtPassword"
                                                                     ErrorMessage="Password Not Match" 
                                                                     ToolTip="Password must be the same" >
                                                                </asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtNama"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator3"
                                                                    ControlToValidate="txtNama"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />

                                                                <asp:RegularExpressionValidator
                                                                    ID="RegularExpressionValidator3"
                                                                    runat="server"
                                                                    ErrorMessage="Only characters allowed"
                                                                    ControlToValidate="txtNama"
                                                                    ValidationExpression="([A-Za-z])+( [A-Za-z]+)*" >
                                                                </asp:RegularExpressionValidator>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtAlamat"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator7"
                                                                    ControlToValidate="txtAlamat"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Email</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtEmail"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator9"
                                                                    ControlToValidate="txtEmail"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                                                <asp:RegularExpressionValidator 
                                                                    ID="regexEmailValid" 
                                                                    runat="server" 
                                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                                    ControlToValidate="txtEmail" 
                                                                    ErrorMessage="Invalid Email Format">
                                                                </asp:RegularExpressionValidator>                                              
                                            </div>
                                        </div>
        
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Telepon</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtTelepon"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator8"
                                                                    ControlToValidate="txtTelepon"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator5" 
                                                                    runat="server"
                                                                    ControlToValidate="txtTelepon"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis Kelamin</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:RadioButtonList Style="margin-top:10px; margin-bottom:10px" ID="rbGender" runat="server" Text='<%# Bind("jenisKelamin") %>'>
                                                    <asp:ListItem Selected="True">Laki-Laki</asp:ListItem>
                                                    <asp:ListItem>Perempuan</asp:ListItem>
                                                </asp:RadioButtonList>
                                                </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"
                                                                ControlToValidate="rbGender" ErrorMessage="Select Gender" Text="Select Gender">
                                                            </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Role</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:DropDownList
                                                    id="ddl_Role"
                                                    class="form-control"
                                                    Runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                         
                                    </form>
                                </div>
                            </div>

        <br/><br/>
            <asp:Button ID="btnCreate" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Create" runat="server" OnClick="btnCreate_Click"></asp:Button>

    </div>









        <div id="update" runat="server">
                <div class="col-xl-6">

                                <h4 class="card-title mb-4">Update</h4>
                                <div class="basic-form">
                                    <form>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Username</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtUsernameU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator2"
                                                                    ControlToValidate="txtUsernameU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Password</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtPasswordU"
                                                    class="form-control form-control-user"
                                                    TextMode="Password"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator4"
                                                                    ControlToValidate="txtPasswordU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Confirm Password</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtConfirmPasswordU"
                                                    class="form-control form-control-user"
                                                    TextMode="Password"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator5"
                                                                    ControlToValidate="txtConfirmPasswordU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                                                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                                                     ControlToValidate="txtConfirmPasswordU"
                                                                     CssClass="ValidationError"
                                                                     ControlToCompare="txtPasswordU"
                                                                     ErrorMessage="Password Not Match" 
                                                                     ToolTip="Password must be the same" >
                                                                </asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Nama</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtNamaU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator12"
                                                                    ControlToValidate="txtNamaU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />

                                                                <asp:RegularExpressionValidator
                                                                    ID="RegularExpressionValidator1"
                                                                    runat="server"
                                                                    ErrorMessage="Only characters allowed"
                                                                    ControlToValidate="txtNamaU"
                                                                    ValidationExpression="([A-Za-z])+( [A-Za-z]+)*" >
                                                                </asp:RegularExpressionValidator>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtAlamatU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator13"
                                                                    ControlToValidate="txtAlamatU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                               
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Email</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtEmailU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                 </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator14"
                                                                    ControlToValidate="txtEmailU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator2" 
                                                                    runat="server" 
                                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                                    ControlToValidate="txtEmailU" 
                                                                    ErrorMessage="Invalid Email Format">
                                                                </asp:RegularExpressionValidator>                                              
                                            </div>
                                        </div>
        
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Telepon</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:TextBox
                                                    id="txtTeleponU"
                                                    class="form-control form-control-user"
                                                    Runat="server" />
                                                </div>
                                                                <asp:RequiredFieldValidator
                                                                    id="RequiredFieldValidator15"
                                                                    ControlToValidate="txtTeleponU"
                                                                    Text="(Required)"
                                                                    ValidationGroup="frmAdd"
                                                                    Runat="server" />
            
                                                                <asp:RegularExpressionValidator 
                                                                    ID="RegularExpressionValidator4" 
                                                                    runat="server"
                                                                    ControlToValidate="txtTeleponU"
                                                                    ErrorMessage="Only numeric allowed"
                                                                    Text="Only Numeric"
                                                                    ValidationExpression="^[0-9]*$"/>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Jenis Kelamin</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:RadioButtonList Style="margin-top:10px; margin-bottom:10px" ID="rbGenderU" runat="server" Text='<%# Bind("jenisKelamin") %>'>
                                                    <asp:ListItem Selected="True">Laki-Laki</asp:ListItem>
                                                    <asp:ListItem>Perempuan</asp:ListItem>
                                                </asp:RadioButtonList>
                                                </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"
                                                                ControlToValidate="rbGenderU" ErrorMessage="Select Gender" Text="Select Gender">
                                                            </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Role</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                 <asp:DropDownList
                                                    id="ddl_RoleU"
                                                    class="form-control"
                                                    Runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Status</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                <asp:RadioButtonList Style="margin-top:10px; margin-bottom:10px" ID="rbStatusU" runat="server" Text='<%# Bind("statusBenih") %>'>
                                                    <asp:ListItem value="1" Selected="True">Aktif</asp:ListItem>
                                                    <asp:ListItem Value="0" >Tidak Aktif</asp:ListItem>
                                                </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                         
                                    </form>
                                </div>
                            </div>
        <br/><br/>
            <asp:Button ID="btnUpdate" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" Text="Update" runat="server" OnClick="btnUpdate_Click"></asp:Button>
            
    </div>

    





    <br/><br/>

    </div>
    </form>
</asp:Content>

